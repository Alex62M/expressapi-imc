const express = require('express')
const { createUser, findAllUsers, finduserById, deleteUser, updateUser } = require('../controllers/controllerUser')
const { postPoids, findAllPoids, findPoidsById,deletePoids } = require('../controllers/controllerPoids')
const router = express.Router()


router.post('/users', createUser)
router.get('/users', findAllUsers)
router.get('/users/:id', finduserById)
router.put('/users/:id', updateUser)
router.delete('/users/:id', deleteUser)
router.post('/poids', postPoids)
router.get('/poids', findAllPoids)
router.get('/poids/:id', findPoidsById)
router.delete('/poids:id',deletePoids)

module.exports = router
