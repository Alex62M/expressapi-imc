const express = require('express');
const app = express();

const sequelize = require('./config/sequelize');

const { success, generateUniqueID } = require('./helpers');

const bodyParser = require('body-parser');
const morgan = require("morgan");

const userRoute = require('./routes/routes');


sequelize.initDb();


app.use(bodyParser.json()).use(morgan("dev"));
app.use('/', userRoute);


app.listen(8080, () => {
    console.log("Server is running at http://localhost:8080");
})