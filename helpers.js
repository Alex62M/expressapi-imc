exports.success = (message, data) => {
    return {
        message: message,
        data: data,
    };
};

exports.generateUniqueID = (users) => {
    const usersIds = users.map(user => user.id);
    const maxId = usersIds.reduce((a, b) => Math.max(a, b));
    const uniqueID = maxId + 1;
    return uniqueID;
}