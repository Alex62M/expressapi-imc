module.exports = (sequelize, Datatypes) => {
    return sequelize.define("UsersIMC", {
        id: {
            type: Datatypes.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        username: {
            type: Datatypes.STRING,
            unique: { msg: "Username existe déjà." }
        },
        password: {
            type: Datatypes.STRING,
            allowNull: false
        },
        age: {
            type: Datatypes.INTEGER,
            allowNull: false
        },
        taille: {
            type: Datatypes.INTEGER,
            allowNull: false
        }
        
    },
        {
            timestamps: true,
            createdAt: false,
            updatedAt: false
        })
}