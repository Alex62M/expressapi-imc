module.exports = (sequelize, Datatypes) => {
    return sequelize.define("PoidsIMC", {
        idp: {
            type: Datatypes.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        valeurp: {
            type: Datatypes.INTEGER,
        },
        datep: {
            type: Datatypes.STRING,
            allowNull: false
        },
        iduser: {
            type: Datatypes.INTEGER,
            allowNull: false
        }
    },
        {
            timestamps: true,
            createdAt: false,
            updatedAt: false
        })
}