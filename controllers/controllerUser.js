const { UsersIMC } = require('../config/sequelize');
const bcrypt = require("bcrypt");

exports.createUser = (req, res) => {
    bcrypt.hash(req.body.password, 10).then((pass) => {
        UsersIMC.create({
            username: req.body.username,
            password: pass,
            age: req.body.age,
            taille: req.body.taille,
            poids: req.body.poids
        }).then((user) => {
            const message = `L'utilisateur ${user.username} a bien été créé.`
            res.json({ message, data: user })
        })
    })
}

exports.findAllUsers = (req, res) => {
    UsersIMC.findAll().then(users => {
        const message = `La liste des utilisateurs a été récupérée.`
        res.json({ message, data: users })
    })
}

exports.finduserById = (req, res) => {
    UsersIMC.findByPk(req.params.id).then(user => {
        const message = `L'utilisateur ${user.username} a été trouvé.`
        res.json({ message, data: user })
    })
}

exports.deleteUser = (req, res) => {
    UsersIMC.findByPk(req.params.id).then((user) => {
        const userDeleted = user;
        UsersIMC.destroy({
            where: { id: user.id }
        }).then(() => {
            const message = `L'utilisateur ${user.username} a été supprimé.`
            res.json({ message, data: user })
        })
    })
}

exports.updateUser = (req, res) => {
    const id = req.params.id;
    UsersIMC.update(req.body, {
        where: { id: id }
    }).then(() => {
        UsersIMC.findByPk(id).then(user => {
            const message = `L'utilisateur ${user.username} a bien été modifié.`
            res.json({ message, data: user })
        })
    })
}