const { PoidsIMC } = require('../config/sequelize');
// const bcrypt = require("bcrypt");

exports.postPoids = (req, res) => {
    console.log("req "+ req.body);
        PoidsIMC.create({
            valeurp: req.body.valeurp,
            datep: req.body.datep,
            iduser: req.body.iduser
        }).then((p) => {
            const message = `La saisie de poids ${p.iduser} a bien été créé.`
            res.json({ message, data: p })
        })
}

exports.findAllPoids = (req, res) => {
    PoidsIMC.findAll().then(poids => {
        const message = `La liste des saisies de poids a été récupérée.`
        res.json({ message, data: poids })
    })
}

exports.findPoidsById = (req, res) => {
    PoidsIMC.findByPk(req.params.id).then(p => {
        const message = `L'utilisateur ${p.iduser} a été trouvé.`
        res.json({ message, data: p })
    })
}

exports.deletePoids = (req, res) => {
    console.log("req "+ req.body);
    PoidsIMC.findByPk(req.params.id).then((p) => {
        const poidsDeleted = p;
        PoidsIMC.destroy({
            where: { idp: p.idp }
        }).then(() => {
            const message = `La saisie de poids de ${p.iduser} a été supprimée.`
            res.json({ message, data: p })
        })
    })
}

