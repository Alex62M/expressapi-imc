const { Sequelize, DataTypes } = require('sequelize');
const UserModel = require('../models/user');
const PoidsModel = require('../models/poids');
const users = require('../db/users.json');
const poids = require('../db/poids.json');
const bcrypt = require("bcrypt");

const sequelize = new Sequelize('UsersIMC', 'root', '', {
    host: 'localhost',
    dialect: 'mariadb',
    logging: false
})

const UsersIMC = UserModel(sequelize, DataTypes);
const PoidsIMC = PoidsModel(sequelize, DataTypes);

const initDb = () => {
    return sequelize.sync({ force: true }).then(_ => {
        users.map(user => {
            bcrypt.hash(user.password, 10).then((pass) => {
                UsersIMC.create({
                    username: user.username,
                    password: pass,
                    age: user.age,
                    taille: user.taille
                });
            })
        })
        console.log(`La BDD a bien été initialisée.`)
    }).then(_ => {
        poids.map(p => {
                PoidsIMC.create({
                    idp: p.idp,
                    valeurp: p.valeurp,
                    datep: p.datep,
                    iduser: p.iduser
                });
        })
        console.log(`La BDD a bien été initialisée.`)
    })
}

module.exports = { initDb, UsersIMC, PoidsIMC }